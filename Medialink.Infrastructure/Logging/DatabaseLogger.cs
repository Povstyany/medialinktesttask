﻿using MediaLink.Domain.Interface;
using MediaLink.Entities;
using MediaLink.Services;

namespace MediaLink.Infrastructure.Logging
{
    //NOTE: Usually logger contains much more fuctionallity (levels, formatters, etc.), like
    // https://github.com/staff0rd/entityframework-logging/blob/master/src/Atquin.EntityFramework.Logging/EntityFrameworkLogger.cs ,
    // but here I put the most simple version to show interaction of service (ILogger) with repository

    public class DatabaseLogger : ILogger
    {
        private readonly IRepository<LogMessage> _logMessageRepository;

        public DatabaseLogger(
                IRepository<LogMessage> logMessageRepository
            )
        {
            _logMessageRepository = logMessageRepository;
        }

        public void Log(LogLevel level, LogMessage message)
        {
            _logMessageRepository.Insert(message);
        }
    }
}
