﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medialink.Infrastructure.Data
{
    public class DapperConnectionParams
    {
        public string ConnectionString { get; set; }
    }
}
