﻿using Dapper;
using Medialink.Infrastructure.Data;
using MediaLink.Domain.Interface;
using MediaLink.Entities;
using System.Data;
using System.Data.SqlClient;

namespace MediaLink.Infrastructure.Data
{
    //NOTE: It's a good practice to have UnitOfWork for repositories, but it makes sense when
    //repository count is big. Was decided not to add UnitOfWork for this task

    public class LogMessageRepository : IRepository<LogMessage>
    {
        private readonly string _connectionString;

        public LogMessageRepository(DapperConnectionParams databaseParams)
        {
            _connectionString = databaseParams.ConnectionString;
        }

        public void Insert(LogMessage entity)
        {
            //NOTE: In some cases large amount of opening/closing db connections can lead to performance issues,
            //but here SQL connection pooling might handle it.
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sql = "INSERT INTO Logs(LogMessage) VALUES(@LogMessage)";
                db.Execute(sql, new { LogMessage = entity.Message });
            }
        }
    }
}
