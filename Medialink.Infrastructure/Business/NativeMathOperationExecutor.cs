﻿using MediaLink.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medialink.Infrastructure.Business
{

    //NOTE: this class just contains operations that initially were inside MathController methods

    public class NativeMathOperationExecutor : IMathOperationExecutor
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Multiply(int a, int b)
        {
            return a * b;
        }

        public int Divide(int a, int b)
        {
            return a / b;
        }
    }
}
