﻿using MediaLink.Entities;
using MediaLink.Services;
using RestSharp;
using System;

namespace MediaLink.Business.Infrastructure
{
    public class MathWebClient : IMathOperationExecutor
    {
        private readonly ILogger _logger;
        private readonly IRestClient _restClient;

        public MathWebClient(
                ILogger logger,
                IRestClient restClient
            )
        {
            _logger = logger;
            _restClient = restClient;
        }

        public int Add(int a, int b)
        {
            string sum = _restClient.Get(new RestRequest($"add?a={a}&b={b}", Method.GET)).Content;

            _logger.Log(LogLevel.Info, new LogMessage() { Message = $"Added values: {a} and {b}" });

            return Convert.ToInt32(sum);
        }

        public int Multiply(int a, int b)
        {
            string product = _restClient.Get(new RestRequest($"multiply?a={a}&b={b}", Method.GET)).Content;

            _logger.Log(LogLevel.Info, new LogMessage() { Message = $"Multiplied values: {a} and {b}" });

            return Convert.ToInt32(product);
        }

        public int Divide(int a, int b)
        {
            string quotient = _restClient.Get(new RestRequest($"divide?a={a}&b={b}", Method.GET)).Content;

            _logger.Log(LogLevel.Info, new LogMessage() { Message = $"Divided values: {a} and {b}" });

            return Convert.ToInt32(quotient);
        }
    }
}
