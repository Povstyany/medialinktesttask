﻿using Medialink.Api.Controllers;
using Medialink.Infrastructure.Business;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;

namespace Medialink.Tests
{
    [TestFixture]
    public class MathControllerTests
    {
        [Test]
        public void TestAdd()
        {
            var operationExecutor = new NativeMathOperationExecutor();
            var mathController = new MathController(operationExecutor);

            HttpResponseMessage result = mathController.Add(4, 2);
            
            Assert.Multiple(async () =>
            {
                Assert.AreEqual(6.ToString(), await result.Content.ReadAsStringAsync());
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            });
        }

        [Test]
        public void TestMultiply()
        {
            var operationExecutor = new NativeMathOperationExecutor();
            var mathController = new MathController(operationExecutor);

            HttpResponseMessage result = mathController.Multiply(4, 2);

            Assert.Multiple(async () =>
            {
                Assert.AreEqual(8.ToString(), await result.Content.ReadAsStringAsync());
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            });
        }

        [Test]
        public void TestDivide()
        {
            var operationExecutor = new NativeMathOperationExecutor();
            var mathController = new MathController(operationExecutor);

            HttpResponseMessage result = mathController.Divide(4, 2);
            
            Assert.Multiple(async () =>
            {
                Assert.AreEqual(2.ToString(), await result.Content.ReadAsStringAsync());
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            });
        }

        [Test]
        public void TestDivideByZero()
        {
            var operationExecutor = new NativeMathOperationExecutor();
            var mathController = new MathController(operationExecutor);

            //NOTE: Exception in WebApi controller leads to 500 status code
            Assert.That(() =>
                mathController.Divide(1, 0),
                Throws.TypeOf<DivideByZeroException>()
                );
        }
    }
}
