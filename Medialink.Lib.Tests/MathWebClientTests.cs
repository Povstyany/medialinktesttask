﻿using MediaLink.Business.Infrastructure;
using MediaLink.Entities;
using MediaLink.Services;
using Moq;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace Medialink.Tests
{
    //NOTE: Log record existance after test could be implemented by mocking methods,
    //but in order to check that method logs some value, it easier to have such implementation as lower

    public class MockingLogger : ILogger
    {
        public List<LogMessage> Logs = new List<LogMessage>();

        public MockingLogger()
        {

        }

        public void Log(LogLevel level, LogMessage message)
        {
            Logs.Add(message);
        }
    }

    [TestFixture]
    public class MathWebClientTests
    {
        //NOTE: these tests do not hanlde Int32 overflow
        
        private IRestClient GetMockedRestClient(int operationResult)
        {
            var restClientMock = new Mock<IRestClient>();

            // NOTE: Get() that called in MathWebClient, under the hood is just an extension method which call Execute()
            // So, we can mock the real Execute() method
            // Source: https://github.com/restsharp/RestSharp/blob/master/RestSharp/RestClientExtensions.cs : 247

            restClientMock
                    .Setup(c => c.Execute(It.IsAny<IRestRequest>(), Method.GET))
                    .Returns((IRestRequest x, Method method) =>
                            {
                                return new RestResponse() { Content = operationResult.ToString() };
                     });

            return restClientMock.Object;
        }

        [Test]
        public void TestAddRange(
            [Range(1, 5, 1)] int a,
            [Range(1, 5, 1)] int b)
        {
            var logger = new MockingLogger();
            var restClient = GetMockedRestClient(a + b);
            var client = new MathWebClient(
                                logger,
                                restClient
                          );

            var clientResult = client.Add(a, b);

            var firstLog = logger.Logs.FirstOrDefault();
            if (firstLog == null)
            {
                Assert.Fail("No log detected");
            }

            Assert.Multiple(() =>
            {
                Assert.AreEqual($"Added values: {a} and {b}", firstLog.Message);
                Assert.AreEqual(a + b, clientResult);
            });

        }

        [Test]
        public void TestMultiplyRange(
            [Range(1, 5, 1)] int a,
            [Range(1, 5, 1)] int b)
        {
            var logger = new MockingLogger();
            var restClient = GetMockedRestClient(a * b);
            var client = new MathWebClient(
                                logger,
                                restClient
                          );

            var clientResult = client.Multiply(a, b);

            Assert.AreEqual(a * b, clientResult);
        }

        [Test]
        public void TestDivideRange(
            [Range(1, 5, 1)] int a,
            [Range(1, 5, 1)] int b)
        {
            var logger = new MockingLogger();
            var restClient = GetMockedRestClient(a / b);
            var client = new MathWebClient(
                                logger,
                                restClient
                          );

            var clientResult = client.Divide(a, b);

            Assert.AreEqual(a / b, clientResult);
        }

    }

}
