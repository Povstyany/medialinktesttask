﻿using MediaLink.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Medialink.Api.Controllers
{
    public class MathController : ApiController
    {
        private readonly IMathOperationExecutor _operationExecutor;

        public MathController(
                IMathOperationExecutor operationExecutor
            )
        {
            _operationExecutor = operationExecutor;
        }

        public HttpResponseMessage Add(int a, int b)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(Convert.ToString(_operationExecutor.Add(a, b)));

            return response;
        }

        public HttpResponseMessage Multiply(int a, int b)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(Convert.ToString(_operationExecutor.Multiply(a, b)));

            return response;
        }

        public HttpResponseMessage Divide(int a, int b)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(Convert.ToString(_operationExecutor.Divide(a, b)));

            return response;
        }
    }
}
