﻿using Autofac;
using Medialink.Infrastructure.Business;
using Medialink.Infrastructure.Data;
using MediaLink.Business.Infrastructure;
using MediaLink.Domain.Interface;
using MediaLink.Entities;
using MediaLink.Infrastructure.Data;
using MediaLink.Infrastructure.Logging;
using MediaLink.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Medialink.Api.DI
{
    public class CoreRegistrationsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // NOTE: Switch registration to NativeMathOperationExecutor in order to execute operations like it was 
            // in initial MathController implementation:
            //builder.RegisterType<NativeMathOperationExecutor>().As<IMathOperationExecutor>();
            builder.RegisterType<MathWebClient>().As<IMathOperationExecutor>();
            
            builder.Register(c => new DapperConnectionParams() { ConnectionString = "connection_string_from_ConfigurationManager" } ).As<DapperConnectionParams>();
            builder.RegisterType<LogMessageRepository>().As<IRepository<LogMessage>>();

            // NOTE: url of webservice may be taken from some configuration file
            builder.Register(c => new RestClient("http://api.mathweb.com/")).As<IRestClient>();
            builder.RegisterType<DatabaseLogger>().As<ILogger>();
        }
    }
}