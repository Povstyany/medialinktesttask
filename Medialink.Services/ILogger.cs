﻿using MediaLink.Entities;

namespace MediaLink.Services
{
    public interface ILogger
    {
        void Log(LogLevel level, LogMessage message);
    }
}
