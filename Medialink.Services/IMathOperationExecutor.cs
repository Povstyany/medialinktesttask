﻿namespace MediaLink.Services
{
    public interface IMathOperationExecutor
    {
        int Add(int a, int b);
        int Multiply(int a, int b);
        int Divide(int a, int b);
    }
}
